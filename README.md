# grpc-practice
Learn to use gRPC in java!

## gRPC Code generation

* m2e users:
`maven generate-sources`

The generated gRPC files will be inside `/target/generated-sources/protobuf`.

Remember to add the folders to java build path.

## Service methods
Refers to `GreetingServerHandlerImpl.java`
* sendMsg: 

    unary request - unary response

* sendStreamMsg:
    
    Stream request - unary response

* getStreamReply:

    unary request - stream response

* streamMsgNReply:

    stream request - stream response

## References

https://grpc.io/docs/languages/java/basics/

https://www.tutorialspoint.com/grpc/index.htm