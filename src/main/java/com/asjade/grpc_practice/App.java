package com.asjade.grpc_practice;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App 
{
	private static final Logger logger = LoggerFactory.getLogger(App.class);
	
    public static void main( String[] args ) throws IOException, InterruptedException {
    	Runnable runner = new Runnable() {
			@Override
			public void run() {
				int port = 8888;
		        try {
		        	GreetingServerHandlerImpl handler = new GreetingServerHandlerImpl();
			    	GreetingServer server = new GreetingServer(port, handler);
			    	logger.info("Starting gRPC server");
					server.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
    	};
    	Thread t = new Thread(runner);
    	t.start();
    	Thread.sleep(1000000);
    }
}
