package com.asjade.grpc_practice;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.asjade.grpc_practice.Greeting.GreetRequest;
import com.asjade.grpc_practice.Greeting.GreetResponse;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

public class GreetingServer {
	private static final Logger logger = LoggerFactory.getLogger(GreetingServer.class);
	private IGreetingServerHandler handlerImpl;
	private int port;
	private Server server;
	
	public GreetingServer(int port, IGreetingServerHandler handlerImpl) {
		this.port = port;
		this.handlerImpl = handlerImpl;
		this.server = ServerBuilder.forPort(this.port).addService(new GreetingService(this.handlerImpl)).build();
	}
	
	public void start() throws IOException {
		this.server.start();
		logger.warn("Server starting..");
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				logger.warn("Shutting down gRPC server..");
				try {
	               server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
	               } catch (InterruptedException e) {
	                  e.printStackTrace(System.err);
	            }
			}
		});
	}
	
	public void stop() {
		if (this.server != null) {
			this.server.shutdown();
		}
	}
	
	
	private class GreetingService extends GreetingServiceGrpc.GreetingServiceImplBase {
		private IGreetingServerHandler handler;
		
		public GreetingService(IGreetingServerHandler handler) {
			this.handler = handler;
		}

		@Override
		public void sendMsg(GreetRequest request, StreamObserver<GreetResponse> responseObserver) {
			this.handler.sendMsg(request, responseObserver);
		}

		@Override
		public StreamObserver<GreetRequest> sendStreamMsg(StreamObserver<GreetResponse> responseObserver) {
			return this.handler.sendStreamMsg(responseObserver);
		}

		@Override
		public void getStreamReply(GreetRequest request, StreamObserver<GreetResponse> responseObserver) {
			this.handler.getStreamReply(request, responseObserver);
		}

		@Override
		public StreamObserver<GreetRequest> streamMsgNReply(StreamObserver<GreetResponse> responseObserver) {
			return this.handler.streamMsgNReply(responseObserver);
		}
		
	}
}
