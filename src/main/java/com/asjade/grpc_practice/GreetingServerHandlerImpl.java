package com.asjade.grpc_practice;

import java.util.ArrayList;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.asjade.grpc_practice.Greeting.GreetRequest;
import com.asjade.grpc_practice.Greeting.GreetResponse;

import io.grpc.stub.StreamObserver;

public class GreetingServerHandlerImpl implements IGreetingServerHandler {
	private static final Logger logger = LoggerFactory.getLogger(GreetingServerHandlerImpl.class);
	private ArrayList<GreetResponse> dummyReponses = new ArrayList<GreetResponse>();
	
	public GreetingServerHandlerImpl() {
		for(Integer i = 0; i < 10; i++) {
			GreetResponse res = GreetResponse.newBuilder().setReply("Greeting " + i.toString()).build();
			dummyReponses.add(res);
		}
	}

	public void sendMsg(GreetRequest request, StreamObserver<GreetResponse> responseObserver) {
		logger.info("sendMsg invoked..");
		String msg = request.getMsg();
		GreetResponse res = GreetResponse.newBuilder().setReply("Got: " + msg).build();
		responseObserver.onNext(res);
		responseObserver.onCompleted();
	}

	public StreamObserver<GreetRequest> sendStreamMsg(StreamObserver<GreetResponse> responseObserver) {
		logger.info("sendStreamMsg invoked..");
		return new StreamObserver<GreetRequest>(){
			GreetResponse validResponse = GreetResponse.newBuilder().build();
			
			public void onNext(GreetRequest request) {
				String msg = request.getMsg();
				for (GreetResponse res : dummyReponses) {
					if (res.getReply().equals(msg)) {
						logger.info("Found valid message in dummyReplies: " + msg);
						validResponse = validResponse.toBuilder().setReply(msg).build();
						
						break;
					}
				}
				if (validResponse.getReply() == null) {
					logger.info("Not found valid message");
				}
			}

			public void onError(Throwable t) {
				logger.info("Error in sendStreamMsg: " + t);
			}

			public void onCompleted() {
				responseObserver.onNext(validResponse);
				responseObserver.onCompleted();
			}
			
		};
	}

	public void getStreamReply(GreetRequest request, StreamObserver<GreetResponse> responseObserver) {
		logger.info("sendStreamReply invoked..");
		logger.info("Acquiring message: " + request.getMsg());
		logger.info("Streaming dummyReplies");
		for (GreetResponse res: dummyReponses) {
			responseObserver.onNext(res);
		}
		responseObserver.onCompleted();
	}

	public StreamObserver<GreetRequest> streamMsgNReply(StreamObserver<GreetResponse> responseObserver) {
		logger.info("streamMsgNReply invoked..");
		return new StreamObserver<GreetRequest>(){
			LinkedList<GreetResponse> validResponses = new LinkedList<GreetResponse>();
			
			public void onNext(GreetRequest request) {
				logger.info("Searching for valid messages..");
				for (GreetResponse res: dummyReponses) {
					if (request.getMsg().equals(res.getReply())) {
						validResponses.add(res);
					}
				}
				if (validResponses.size() > 0) {
					responseObserver.onNext(validResponses.pop());
				} else {
					GreetResponse fakeResponse = GreetResponse.newBuilder().setReply(request.getMsg() + " not found").build();
					responseObserver.onNext(fakeResponse);
				}
				
			}

			public void onError(Throwable t) {
				logger.info("Error in streamMsgNReply: " + t);
				
			}

			public void onCompleted() {
				responseObserver.onCompleted();
			}
			
		};
	}

}
