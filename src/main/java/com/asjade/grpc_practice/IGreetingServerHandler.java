package com.asjade.grpc_practice;

import com.asjade.grpc_practice.Greeting.GreetRequest;
import com.asjade.grpc_practice.Greeting.GreetResponse;

import io.grpc.stub.StreamObserver;

public interface IGreetingServerHandler {

	void sendMsg(GreetRequest request, StreamObserver<GreetResponse> responseObserver);

	StreamObserver<GreetRequest> sendStreamMsg(StreamObserver<GreetResponse> responseObserver);

	void getStreamReply(GreetRequest request, StreamObserver<GreetResponse> responseObserver);

	StreamObserver<GreetRequest> streamMsgNReply(StreamObserver<GreetResponse> responseObserver);
}