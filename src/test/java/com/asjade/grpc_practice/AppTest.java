package com.asjade.grpc_practice;

import java.io.IOException;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.asjade.grpc_practice.Greeting.GreetRequest;
import com.asjade.grpc_practice.Greeting.GreetResponse;
import com.asjade.grpc_practice.GreetingServiceGrpc.GreetingServiceBlockingStub;
import com.asjade.grpc_practice.GreetingServiceGrpc.GreetingServiceStub;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class AppTest {
	private static final Logger logger = LoggerFactory.getLogger(AppTest.class);
	private static int port = 8888;
	private static String host = "127.0.0.1";
	// Server
	private GreetingServerHandlerImpl handler = new GreetingServerHandlerImpl();
	private GreetingServer server = new GreetingServer(port, handler);
	// Client stub
	private static ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
	private GreetingServiceBlockingStub blockingStub = GreetingServiceGrpc.newBlockingStub(channel);
	private GreetingServiceStub stub = GreetingServiceGrpc.newStub(channel);
	
	@Before
	public void startServer() throws InterruptedException {
		Runnable r = new Runnable() {
			@Override
			public void run() {
		        try {
			    	logger.info("Starting gRPC server");
					server.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
    	};
    	Thread t = new Thread(r);
    	t.start();
    	Thread.sleep(1000);
	}
	
	@After
	public void stopServer() throws InterruptedException {
		server.stop();
		Thread.sleep(1000);
	}
	
	@Test
	public void testSendMsg() throws InterruptedException {
		logger.info("Start testing sendMsg");
		
		Runnable r = new Runnable() {

			@Override
			public void run() {
				GreetRequest request = GreetRequest.newBuilder().setMsg("Greet from testSendMsg").build();
				try {
					GreetResponse response = blockingStub.sendMsg(request);
					System.out.println(response.getReply());
				} catch(Exception e) {
					logger.warn(e.getMessage());
				}
			}
			
		};
		
		Thread t = new Thread(r);
		t.start();
		Thread.sleep(300);
	}
	
	@Test
	public void testSendStreamMsg() throws InterruptedException {
		logger.info("Start testing sendStreamMsg");
		
		Runnable r = new Runnable() {

			@Override
			public void run() {
				StreamObserver<GreetRequest> streamSender = stub.sendStreamMsg(new StreamObserver<GreetResponse>() {

					@Override
					public void onNext(GreetResponse response) {
						logger.info(response.getReply());
					}

					@Override
					public void onError(Throwable t) {
						logger.error(t.getMessage());
						
					}

					@Override
					public void onCompleted() {
						logger.info("Complete");
						
					}});
				
				for (Integer i = 3; i < 13; i++) {
					GreetRequest request = GreetRequest.newBuilder().setMsg("Greeting " + i.toString()).build();
					try {
						streamSender.onNext(request);
					} catch (Exception e) {
						logger.warn(e.getMessage());
					}
					
				}
				streamSender.onCompleted();
			}
			
		};
		Thread t = new Thread(r);
		t.start();
		Thread.sleep(300);
	}
	
	@Test
	public void testGetStreamReply() throws InterruptedException {
		Runnable r = new Runnable() {

			@Override
			public void run() {
				GreetRequest request = GreetRequest.newBuilder().setMsg("Whatever").build();
				try {
					Iterator<GreetResponse> response = blockingStub.getStreamReply(request);
					while(response.hasNext()) {
						System.out.println(response.next().getReply());
					}
				} catch(Exception e) {
					logger.warn(e.getMessage());
				}
			}
		};
		
		Thread t = new Thread(r);
		t.start();
		Thread.sleep(300);
	}
	
	@Test
	public void testSendStreamNReply() throws InterruptedException {
		Runnable r = new Runnable() {

			@Override
			public void run() {
				StreamObserver<GreetRequest> streamSender = stub.streamMsgNReply(new StreamObserver<GreetResponse>() {

					@Override
					public void onNext(GreetResponse response) {
						logger.info("Client: " + response.getReply());
					}

					@Override
					public void onError(Throwable t) {
						logger.error(t.getMessage());
						
					}

					@Override
					public void onCompleted() {
						logger.info("Complete");
						
					}
					
				});
				for (Integer i = 3; i < 13; i++) {
					GreetRequest request = GreetRequest.newBuilder().setMsg("Greeting " + i.toString()).build();
					try {
						streamSender.onNext(request);
					} catch (Exception e) {
						logger.warn(e.getMessage());
					}
					
				}
				streamSender.onCompleted();
			}
		};
		
		Thread t = new Thread(r);
		t.start();
		Thread.sleep(300);
	}
}
