package com.asjade.grpc_practice;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.13.1)",
    comments = "Source: greeting.proto")
public final class GreetingServiceGrpc {

  private GreetingServiceGrpc() {}

  public static final String SERVICE_NAME = "GreetingService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest,
      com.asjade.grpc_practice.Greeting.GreetResponse> getSendMsgMethod;

  public static io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest,
      com.asjade.grpc_practice.Greeting.GreetResponse> getSendMsgMethod() {
    io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest, com.asjade.grpc_practice.Greeting.GreetResponse> getSendMsgMethod;
    if ((getSendMsgMethod = GreetingServiceGrpc.getSendMsgMethod) == null) {
      synchronized (GreetingServiceGrpc.class) {
        if ((getSendMsgMethod = GreetingServiceGrpc.getSendMsgMethod) == null) {
          GreetingServiceGrpc.getSendMsgMethod = getSendMsgMethod = 
              io.grpc.MethodDescriptor.<com.asjade.grpc_practice.Greeting.GreetRequest, com.asjade.grpc_practice.Greeting.GreetResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "GreetingService", "SendMsg"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.asjade.grpc_practice.Greeting.GreetRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.asjade.grpc_practice.Greeting.GreetResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new GreetingServiceMethodDescriptorSupplier("SendMsg"))
                  .build();
          }
        }
     }
     return getSendMsgMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest,
      com.asjade.grpc_practice.Greeting.GreetResponse> getSendStreamMsgMethod;

  public static io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest,
      com.asjade.grpc_practice.Greeting.GreetResponse> getSendStreamMsgMethod() {
    io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest, com.asjade.grpc_practice.Greeting.GreetResponse> getSendStreamMsgMethod;
    if ((getSendStreamMsgMethod = GreetingServiceGrpc.getSendStreamMsgMethod) == null) {
      synchronized (GreetingServiceGrpc.class) {
        if ((getSendStreamMsgMethod = GreetingServiceGrpc.getSendStreamMsgMethod) == null) {
          GreetingServiceGrpc.getSendStreamMsgMethod = getSendStreamMsgMethod = 
              io.grpc.MethodDescriptor.<com.asjade.grpc_practice.Greeting.GreetRequest, com.asjade.grpc_practice.Greeting.GreetResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "GreetingService", "SendStreamMsg"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.asjade.grpc_practice.Greeting.GreetRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.asjade.grpc_practice.Greeting.GreetResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new GreetingServiceMethodDescriptorSupplier("SendStreamMsg"))
                  .build();
          }
        }
     }
     return getSendStreamMsgMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest,
      com.asjade.grpc_practice.Greeting.GreetResponse> getGetStreamReplyMethod;

  public static io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest,
      com.asjade.grpc_practice.Greeting.GreetResponse> getGetStreamReplyMethod() {
    io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest, com.asjade.grpc_practice.Greeting.GreetResponse> getGetStreamReplyMethod;
    if ((getGetStreamReplyMethod = GreetingServiceGrpc.getGetStreamReplyMethod) == null) {
      synchronized (GreetingServiceGrpc.class) {
        if ((getGetStreamReplyMethod = GreetingServiceGrpc.getGetStreamReplyMethod) == null) {
          GreetingServiceGrpc.getGetStreamReplyMethod = getGetStreamReplyMethod = 
              io.grpc.MethodDescriptor.<com.asjade.grpc_practice.Greeting.GreetRequest, com.asjade.grpc_practice.Greeting.GreetResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "GreetingService", "GetStreamReply"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.asjade.grpc_practice.Greeting.GreetRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.asjade.grpc_practice.Greeting.GreetResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new GreetingServiceMethodDescriptorSupplier("GetStreamReply"))
                  .build();
          }
        }
     }
     return getGetStreamReplyMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest,
      com.asjade.grpc_practice.Greeting.GreetResponse> getStreamMsgNReplyMethod;

  public static io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest,
      com.asjade.grpc_practice.Greeting.GreetResponse> getStreamMsgNReplyMethod() {
    io.grpc.MethodDescriptor<com.asjade.grpc_practice.Greeting.GreetRequest, com.asjade.grpc_practice.Greeting.GreetResponse> getStreamMsgNReplyMethod;
    if ((getStreamMsgNReplyMethod = GreetingServiceGrpc.getStreamMsgNReplyMethod) == null) {
      synchronized (GreetingServiceGrpc.class) {
        if ((getStreamMsgNReplyMethod = GreetingServiceGrpc.getStreamMsgNReplyMethod) == null) {
          GreetingServiceGrpc.getStreamMsgNReplyMethod = getStreamMsgNReplyMethod = 
              io.grpc.MethodDescriptor.<com.asjade.grpc_practice.Greeting.GreetRequest, com.asjade.grpc_practice.Greeting.GreetResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "GreetingService", "StreamMsgNReply"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.asjade.grpc_practice.Greeting.GreetRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.asjade.grpc_practice.Greeting.GreetResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new GreetingServiceMethodDescriptorSupplier("StreamMsgNReply"))
                  .build();
          }
        }
     }
     return getStreamMsgNReplyMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static GreetingServiceStub newStub(io.grpc.Channel channel) {
    return new GreetingServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static GreetingServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new GreetingServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static GreetingServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new GreetingServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class GreetingServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void sendMsg(com.asjade.grpc_practice.Greeting.GreetRequest request,
        io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSendMsgMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetRequest> sendStreamMsg(
        io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(getSendStreamMsgMethod(), responseObserver);
    }

    /**
     */
    public void getStreamReply(com.asjade.grpc_practice.Greeting.GreetRequest request,
        io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetStreamReplyMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetRequest> streamMsgNReply(
        io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(getStreamMsgNReplyMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSendMsgMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.asjade.grpc_practice.Greeting.GreetRequest,
                com.asjade.grpc_practice.Greeting.GreetResponse>(
                  this, METHODID_SEND_MSG)))
          .addMethod(
            getSendStreamMsgMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                com.asjade.grpc_practice.Greeting.GreetRequest,
                com.asjade.grpc_practice.Greeting.GreetResponse>(
                  this, METHODID_SEND_STREAM_MSG)))
          .addMethod(
            getGetStreamReplyMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.asjade.grpc_practice.Greeting.GreetRequest,
                com.asjade.grpc_practice.Greeting.GreetResponse>(
                  this, METHODID_GET_STREAM_REPLY)))
          .addMethod(
            getStreamMsgNReplyMethod(),
            asyncBidiStreamingCall(
              new MethodHandlers<
                com.asjade.grpc_practice.Greeting.GreetRequest,
                com.asjade.grpc_practice.Greeting.GreetResponse>(
                  this, METHODID_STREAM_MSG_NREPLY)))
          .build();
    }
  }

  /**
   */
  public static final class GreetingServiceStub extends io.grpc.stub.AbstractStub<GreetingServiceStub> {
    private GreetingServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GreetingServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GreetingServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GreetingServiceStub(channel, callOptions);
    }

    /**
     */
    public void sendMsg(com.asjade.grpc_practice.Greeting.GreetRequest request,
        io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSendMsgMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetRequest> sendStreamMsg(
        io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getSendStreamMsgMethod(), getCallOptions()), responseObserver);
    }

    /**
     */
    public void getStreamReply(com.asjade.grpc_practice.Greeting.GreetRequest request,
        io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetStreamReplyMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetRequest> streamMsgNReply(
        io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(getStreamMsgNReplyMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   */
  public static final class GreetingServiceBlockingStub extends io.grpc.stub.AbstractStub<GreetingServiceBlockingStub> {
    private GreetingServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GreetingServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GreetingServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GreetingServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.asjade.grpc_practice.Greeting.GreetResponse sendMsg(com.asjade.grpc_practice.Greeting.GreetRequest request) {
      return blockingUnaryCall(
          getChannel(), getSendMsgMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<com.asjade.grpc_practice.Greeting.GreetResponse> getStreamReply(
        com.asjade.grpc_practice.Greeting.GreetRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getGetStreamReplyMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class GreetingServiceFutureStub extends io.grpc.stub.AbstractStub<GreetingServiceFutureStub> {
    private GreetingServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GreetingServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GreetingServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GreetingServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.asjade.grpc_practice.Greeting.GreetResponse> sendMsg(
        com.asjade.grpc_practice.Greeting.GreetRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSendMsgMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SEND_MSG = 0;
  private static final int METHODID_GET_STREAM_REPLY = 1;
  private static final int METHODID_SEND_STREAM_MSG = 2;
  private static final int METHODID_STREAM_MSG_NREPLY = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final GreetingServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(GreetingServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SEND_MSG:
          serviceImpl.sendMsg((com.asjade.grpc_practice.Greeting.GreetRequest) request,
              (io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse>) responseObserver);
          break;
        case METHODID_GET_STREAM_REPLY:
          serviceImpl.getStreamReply((com.asjade.grpc_practice.Greeting.GreetRequest) request,
              (io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SEND_STREAM_MSG:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.sendStreamMsg(
              (io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse>) responseObserver);
        case METHODID_STREAM_MSG_NREPLY:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.streamMsgNReply(
              (io.grpc.stub.StreamObserver<com.asjade.grpc_practice.Greeting.GreetResponse>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class GreetingServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    GreetingServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.asjade.grpc_practice.Greeting.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("GreetingService");
    }
  }

  private static final class GreetingServiceFileDescriptorSupplier
      extends GreetingServiceBaseDescriptorSupplier {
    GreetingServiceFileDescriptorSupplier() {}
  }

  private static final class GreetingServiceMethodDescriptorSupplier
      extends GreetingServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    GreetingServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (GreetingServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new GreetingServiceFileDescriptorSupplier())
              .addMethod(getSendMsgMethod())
              .addMethod(getSendStreamMsgMethod())
              .addMethod(getGetStreamReplyMethod())
              .addMethod(getStreamMsgNReplyMethod())
              .build();
        }
      }
    }
    return result;
  }
}
